import isAlpha from "validator/es/lib/isAlpha";
import isAfter from "validator/es/lib/isAfter";

const addButton = document.getElementById("addButton");

addButton?.addEventListener("click", addDeveloper);

function addDeveloper(){
    const developerName = document.getElementById("developerName").value;
    const developerFoundedDate = document.getElementById("datepicker").value;
    const errorDiv = document.getElementById("errorText");
    let errorText = "";

    if(! isAlpha(developerName, 'en-GB', {ignore: ' '}) || isAfter(developerFoundedDate)){
        errorText = "The name can only contain letters and the founded date must be in the past.";
        errorDiv.hidden = false;
        errorDiv.innerHTML = errorText;
        return;
    }
    const csrfToken = document.querySelector("meta[name=_csrf]").content;
    const csrfHeader = document.querySelector("meta[name=_csrf_header]").content;

    const headers = {
        "Content-Type": "application/json"
    };
    headers[csrfHeader] = csrfToken;

    fetch(`/api/developers`, {
        method: "POST",
        headers,
        body: JSON.stringify({
            name: developerName,
            foundedDate: developerFoundedDate
        })
    }).then(response => {
        if(response.status !== 201){
            errorText = `Something unexpected happened: ${response.status}`;
            errorDiv.hidden = false;
            errorDiv.innerHTML = errorText;
        }else{
            errorDiv.hidden = true;
            const developersTable = document.getElementById("developersTable");
            //TODO: figure out how to get id (max id of games, developers and platforms list) --> hidden input?
            developersTable.innerHTML += `
                <tr>
                    <td>
                        <a class="link-primary text-decoration-none" href="">${developerName}</a>
                    </td>
                    <td>${developerFoundedDate}</td>
                    <td></td>
                    <td>
                        <button class="btn btn-secondary">Delete</button>
                    </td>
                </tr>
            `;
        }
        }
    ).catch(error => alert(error.message));
}

