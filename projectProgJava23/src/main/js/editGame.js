const saveButton = document.getElementById("saveButton");

saveButton?.addEventListener("click", saveChanges);

function saveChanges(){
    //TODO: add release date
    const gameName = document.getElementById("gameName");
    const rating = document.getElementById("rating");
    const gameId = gameName.previousElementSibling.value;
    const errorDiv = document.getElementById("errorText");
    let errorText = "";

    const csrfToken = document.querySelector("meta[name=_csrf]").content;
    const csrfHeader = document.querySelector("meta[name=_csrf_header]").content;

    const headers = {
        "Content-Type": "application/json"
    };
    headers[csrfHeader] = csrfToken;

    fetch(`/api/games/${gameId}`, {
        method: "PUT",
        headers,
        body: JSON.stringify({
            id: gameId,
            name: gameName.innerHTML,
            rating: rating.innerHTML
        })
    }).then(response => {
        if(response.status !== 204){
            errorText = `Something unexpected happened: ${response.status}`;
            errorDiv.hidden = false;
            errorDiv.innerHTML = errorText;
        }
    }).catch(error => alert(error.message));
}

