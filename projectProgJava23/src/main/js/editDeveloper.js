const saveButton = document.getElementById("saveButton");

saveButton?.addEventListener("click", saveChanges);

function saveChanges(){
    //TODO: add founded date
    const developerName = document.getElementById("developerName");
    const developerId = developerName.previousElementSibling.value;
    const errorDiv = document.getElementById("errorText");
    let errorText = "";

    const csrfToken = document.querySelector("meta[name=_csrf]").content;
    const csrfHeader = document.querySelector("meta[name=_csrf_header]").content;

    const headers = {
        "Content-Type": "application/json"
    };
    headers[csrfHeader] = csrfToken;

    fetch(`/api/developers/${developerId}`, {
        method: "PUT",
        headers,
        body: JSON.stringify({
            id: developerId,
            name: developerName.innerHTML
        })
    }).then(response => {
        if(response.status !== 204){
            errorText = `Something unexpected happened: ${response.status}`;
            errorDiv.hidden = false;
            errorDiv.innerHTML = errorText;
        }
    }).catch(error => alert(error.message));
}

