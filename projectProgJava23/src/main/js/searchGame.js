const button = document.getElementById("searchButton");
const nameInput = document.getElementById("name");
const tableBody = document.getElementById("tableBody");
const gamesTable = document.getElementById("gamesTable");

button.addEventListener("click", searchDevelopers);

function searchDevelopers(){
    const searchTerm = nameInput.value;
    const errorDiv = document.getElementById("errorText");
    let errorText = "";

    fetch(`/api/games?name=${searchTerm}`, {
        method: "GET" // GET = default
    })
        .then(response => {
            if(response.status === 200){
                errorDiv.hidden = true;
                gamesTable.hidden = false;
                return response.json();
            }
            else if(response.status === 204){
                errorText = "Nothing was found.";
            }else{
                errorText = `An error has occurred: ${response.status}`;
            }
            if(errorText !== ""){
                gamesTable.hidden = true;
                errorDiv.hidden = false;
                errorDiv.innerText = errorText;
            }
        })
        .then(questions =>{
            processGames(questions);
        });
}

function processGames(gameArray){
    tableBody.innerHTML = "";
    if(gameArray === null) return;
    for (let game of gameArray) {
        tableBody.innerHTML += `
            <tr>
                <th><a href="/games/detailsGame?gameID=${game.id}">${game.name}</a></th>
                <th>${game.rating}</th>
                <th>${game.releaseDate}</th>
            </tr>
        `;
    }
}