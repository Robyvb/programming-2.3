const button = document.getElementById("searchButton");
const nameInput = document.getElementById("name");
const tableBody = document.getElementById("tableBody");
const developerTable = document.getElementById("developerTable");

button.addEventListener("click", searchDevelopers);

function searchDevelopers(){
    const searchTerm = nameInput.value;
    const errorDiv = document.getElementById("errorText");
    let errorText = "";

    fetch(`/api/developers?partOfName=${searchTerm}`, {
        method: "GET" // GET = default
    })
        .then(response => {
            if(response.status === 200){
                errorDiv.hidden = true;
                developerTable.hidden = false;
                return response.json();
            }else if(response.status === 204){
                errorText = "Nothing was found.";
            }else{
                errorText = `An error has occurred: ${response.status}`;
            }
            if(errorText !== ""){
                developerTable.hidden = true;
                errorDiv.hidden = false;
                errorDiv.innerText = errorText;
            }
        })
        .then(questions =>{
            processQuestions(questions);
        });
}

function processQuestions(developerArray){
    tableBody.innerHTML = "";
    for (let developer of developerArray) {
        tableBody.innerHTML += `
            <tr>
                <th><a href="/developers/detailsDeveloper?developerID=${developer.id}">${developer.name}</a></th>
            </tr>
        `;
    }
}