const deleteButtons = document.querySelectorAll("tbody#developersTable button");

async function deleteDeveloper(event){
    const button = event.target;
    const id = button.previousElementSibling.value;
    const errorDiv = document.getElementById("errorText");
    let errorText = "";

    let response;
    try {
        const csrfToken = document.querySelector("meta[name=_csrf]").content;
        const csrfHeader = document.querySelector("meta[name=_csrf_header]").content;

        const headers = {
            "Content-Type": "application/json"
        };
        headers[csrfHeader] = csrfToken;

        response = await fetch (`/api/developers/${id}`, {
            method: "DELETE",
            headers
        });
    } catch (e) {
        errorText = `An error has occured: ${e.message}`;
        errorDiv.hidden = false;
        errorDiv.innerHTML = errorText;
    }

    if (response.status === 204){
        //format: button inside td --> inside tr --> go up 2 parents
        //tr is inside tbody (whole table) --> remove this tr from tbody, 1 parent up
        const removeDiv = button.parentElement.parentElement;
        const parentDiv = removeDiv.parentElement;

        parentDiv.removeChild(removeDiv);
    }else{
        errorText = `Unsupported status code ${response.status}`;
        errorDiv.hidden = false;
        errorDiv.innerHTML = errorText;
    }
}

deleteButtons.forEach(deleteButton =>{
   deleteButton?.addEventListener("click", deleteDeveloper);
});