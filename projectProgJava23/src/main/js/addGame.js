import isAlpha from "validator/es/lib/isAlpha";
import isFloat from "validator/es/lib/isFloat";

const addButton = document.getElementById("addButton");

addButton?.addEventListener("click", addDeveloper);

function addDeveloper(){
    const gameName = document.getElementById("gameName").value;
    const gameRating = document.getElementById("gameRating").value;
    const gameReleaseDate = document.getElementById("datepicker").value;
    const errorDiv = document.getElementById("errorText");
    let errorText = "";

    if(! isAlpha(gameName, 'en-GB', {ignore: ' '}) || ! isFloat(gameRating, {min: 0.1, max: 10})){
        errorText = "Please fill in a name and rating. The name can only contain letters and the rating can only be between 0.1 and 10.";
        errorDiv.hidden = false;
        errorDiv.innerHTML = errorText;
        return;
    }
    const csrfToken = document.querySelector("meta[name=_csrf]").content;
    const csrfHeader = document.querySelector("meta[name=_csrf_header]").content;

    const headers = {
        "Content-Type": "application/json"
    };
    headers[csrfHeader] = csrfToken;

    fetch(`/api/games`, {
        method: "POST",
        headers,
        body: JSON.stringify({
            name: gameName,
            rating: gameRating,
            releaseDate: gameReleaseDate
        })
    }).then(response => {
            if(response.status !== 201){
                errorText = `Something unexpected happened: ${response.status}`;
                errorDiv.hidden = false;
                errorDiv.innerHTML = errorText;
            }else{
                const gamesTable = document.getElementById("gamesTable");
                //TODO: same as in addDeveloper -> get max id from developers,name and platforms table
                gamesTable.innerHTML += `
                    <tr>
                        <td>
                            <a class="link-primary text-decoration-none" href="">${gameName}</a>
                        </td>
                        <td>${parseFloat(gameRating)}</td>
                        <td>${gameReleaseDate}</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                            <button class="btn btn-secondary">Delete</button>
                        </td>
                    </tr>
                `;
            }
        }
    ).catch(error => alert(error.message));
}

