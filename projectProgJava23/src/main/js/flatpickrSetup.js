import flatpickr from "flatpickr";

const datepicker = document.getElementById("datepicker");

flatpickr(datepicker,{
    altInput: true,
    altFormat: "F j, Y",
    dateFormat: "Y-m-d",
});