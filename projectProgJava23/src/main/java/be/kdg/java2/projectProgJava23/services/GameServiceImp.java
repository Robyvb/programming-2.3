package be.kdg.java2.projectProgJava23.services;

import be.kdg.java2.projectProgJava23.domain.Developer;
import be.kdg.java2.projectProgJava23.domain.Game;
import be.kdg.java2.projectProgJava23.domain.Platform;
import be.kdg.java2.projectProgJava23.repositories.DeveloperRepository;
import be.kdg.java2.projectProgJava23.repositories.GameRepository;
import be.kdg.java2.projectProgJava23.repositories.PlatformRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.transaction.PlatformTransactionManagerCustomizer;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.util.Iterator;
import java.util.List;

@Service
public class GameServiceImp implements GameService {
    private final GameRepository gameRepository;
    private final DeveloperRepository developerRepository;
    private final PlatformRepository platformRepository;

    private static final Logger logger = LoggerFactory.getLogger(GameServiceImp.class);
    @Autowired
    //JSON saving goes here too
    public GameServiceImp(GameRepository gameRepository, DeveloperRepository developerRepository, PlatformRepository platformRepository){
        this.gameRepository = gameRepository;
        this.developerRepository = developerRepository;
        this.platformRepository = platformRepository;
    }

    @Override
    public List<Game> showAllGames() {
        return gameRepository.findAll();
    }

    @Override
    public Game addGame(Game game) {
        logger.debug("Adding game:" + game.getName());
        return gameRepository.save(game);
    }

    @Override
    public Game findGameById(int id){
        logger.debug("Finding game by id " + id);
        return gameRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("error"));
    }

    @Override
    public List<Game> addAllGames(List<Game> games) {
        return gameRepository.saveAll(games);
    }

    @Override
    public List<Game> findByRatingGreaterThanEqual(double rating) {
        logger.debug("User is filtering on games by rating higher than or equal to given ");
        if(rating > 10){
            logger.error("User has generated a RatingHigherThanTenException");
        }
        return gameRepository.findByRatingGreaterThanEqual(rating);
    }

    @Override
    public List<Game> findByRatingAndReleaseDateGreaterThanEqual(double rating, LocalDate releaseDate) {
        //only one genre --> converter on text input
        logger.debug("User is filtering on games by rating and release date higher than or equal to given");
        //check if the given date to filter on is in the future
        if(releaseDate.isAfter(LocalDate.now())){
            logger.error("User has generated a DateInFutureException");
        }
        return gameRepository.findByRatingAndReleaseDateGreaterThanEqual(rating, releaseDate);
    }

    @Override
    public List<Game> searchGames(String partOfName){
        return this.gameRepository.findByNameContainsIgnoreCase(partOfName);
    }

    @Override
    public void deleteGame(int id){
        Game gameToBeDeleted = gameRepository.findById(id).orElseThrow();
        //get associated developer to remove
        Developer associatedDeveloper = gameToBeDeleted.getDeveloper();
        List<Platform> associatedPlatforms = gameToBeDeleted.getPlatforms();

        associatedPlatforms.forEach(platform -> {
            logger.debug("Removing: " + platform);
            platform.removeGame(gameToBeDeleted);
            platformRepository.save(platform);
            logger.debug("Removed: " + platform);
        });

        logger.debug("developer: " + associatedDeveloper);
        logger.debug("all developers: " + developerRepository.findAll());
        logger.debug("all games: " + gameRepository.findAll());
        if(associatedDeveloper != null){
            //delete associated developer from game
            gameToBeDeleted.removeDeveloper();
            //delete game from associated developer
            associatedDeveloper.removeGame(gameToBeDeleted);
            logger.debug("After removal: " + associatedDeveloper.getGames());

            //save developer's changes
            developerRepository.save(associatedDeveloper);
            logger.debug("Developer: " + developerRepository.getById(associatedDeveloper.getId()).getGames());
        }
        //delete game
        gameRepository.delete(gameToBeDeleted);
    }

    @Override
    public void updateGame(Game game) {
        gameRepository.save(game);
    }
}
