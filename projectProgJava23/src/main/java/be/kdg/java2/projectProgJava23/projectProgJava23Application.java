package be.kdg.java2.projectProgJava23;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class projectProgJava23Application {

    public static void main(String[] args) {
        SpringApplication.run(projectProgJava23Application.class, args);
    }

}
