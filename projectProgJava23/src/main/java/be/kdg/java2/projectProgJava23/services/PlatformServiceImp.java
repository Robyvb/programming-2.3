package be.kdg.java2.projectProgJava23.services;

import be.kdg.java2.projectProgJava23.domain.Platform;
import be.kdg.java2.projectProgJava23.repositories.PlatformRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class PlatformServiceImp implements PlatformService {
    private PlatformRepository repository;
    private static final Logger logger = LoggerFactory.getLogger(PlatformServiceImp.class);

    @Autowired
    public PlatformServiceImp(PlatformRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Platform> showAllPlatforms() {
        return repository.findAll();
    }

    @Override
    public Platform addPlatform(Platform platform) {
        return repository.save(platform);
    }

    @Override
    public Platform findPlatformById(int id) {
        logger.debug("Finding platform by id " + id);
        return repository.findById(id).orElseThrow(() -> new EntityNotFoundException("error"));
    }
}
