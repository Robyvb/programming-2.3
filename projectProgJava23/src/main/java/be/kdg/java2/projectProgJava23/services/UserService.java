package be.kdg.java2.projectProgJava23.services;

import be.kdg.java2.projectProgJava23.domain.User;
import be.kdg.java2.projectProgJava23.repositories.UserRepository;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User findUser(String username){
        return userRepository.findByUsername(username);
    }
}
