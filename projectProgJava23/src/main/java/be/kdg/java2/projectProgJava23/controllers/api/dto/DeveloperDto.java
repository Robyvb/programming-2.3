package be.kdg.java2.projectProgJava23.controllers.api.dto;

import java.time.LocalDate;

public class DeveloperDto {
    private long id;
    private String name;

    public DeveloperDto() {
    }

    public DeveloperDto(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
