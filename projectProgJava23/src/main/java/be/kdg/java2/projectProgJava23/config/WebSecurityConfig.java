package be.kdg.java2.projectProgJava23.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //@formatter:off
        http
                .httpBasic()
                    .authenticationEntryPoint(httpStatusEntryPoint())
                .and()
                .authorizeRequests()
                    .antMatchers("/", "/search")
                        .permitAll()
                    .antMatchers(HttpMethod.GET, "/api/**/*")
                        .permitAll()
                    .regexMatchers("/(developers|games)")
                        .permitAll()
                    .regexMatchers("/(developers|games)/(searchDeveloper|searchGame)")
                        .permitAll()
                    .regexMatchers(HttpMethod.GET, ".+\\.(css|js|map|woff2?|jpg)(\\?.*)?")
                        .permitAll()
                    .anyRequest()
                        .authenticated()
                    .and()
                .csrf()
                    .and()
                .formLogin()
                    .loginPage("/login")
                        .permitAll()
                    .and()
                .logout()
                    .permitAll();
        //@formatter:on
    }

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder(); // this encoder isn't the most secure but has the most out of the box functions
    }

    private HttpStatusEntryPoint httpStatusEntryPoint() {
        return new HttpStatusEntryPoint(HttpStatus.FORBIDDEN);
    }
}
