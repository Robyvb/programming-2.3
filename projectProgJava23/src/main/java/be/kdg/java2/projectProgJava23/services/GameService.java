package be.kdg.java2.projectProgJava23.services;

import be.kdg.java2.projectProgJava23.domain.Game;

import java.time.LocalDate;
import java.util.List;

public interface GameService {
    List<Game> showAllGames();
    Game addGame(Game game);
    Game findGameById(int id);
    List<Game> addAllGames(List<Game> games);
    List<Game> findByRatingGreaterThanEqual(double rating);
    List<Game> findByRatingAndReleaseDateGreaterThanEqual(double rating, LocalDate releaseDate);
    List<Game> searchGames(String partOfName);
    void deleteGame(int id);
    void updateGame(Game game);
}
