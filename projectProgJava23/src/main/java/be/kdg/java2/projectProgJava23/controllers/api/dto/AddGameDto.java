package be.kdg.java2.projectProgJava23.controllers.api.dto;

import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

public class AddGameDto {
    private String name;
    private double rating;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate releaseDate;

    public AddGameDto() {

    }

    public AddGameDto(String name, double rating, LocalDate releaseDate) {
        this.name = name;
        this.rating = rating;
        this.releaseDate = releaseDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }
}
