package be.kdg.java2.projectProgJava23.bootstrap;

import be.kdg.java2.projectProgJava23.domain.*;
import be.kdg.java2.projectProgJava23.repositories.DeveloperRepository;
import be.kdg.java2.projectProgJava23.repositories.GameRepository;
import be.kdg.java2.projectProgJava23.repositories.PlatformRepository;
import be.kdg.java2.projectProgJava23.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

@Component
@Transactional
public class SeedData implements CommandLineRunner {
    private final Logger logger = LoggerFactory.getLogger(SeedData.class);

    private final DeveloperRepository developerRepository;
    private final GameRepository gameRepository;
    private final PlatformRepository platformRepository;
    private final UserRepository userRepository;

    public SeedData(DeveloperRepository developerRepository, GameRepository gameRepository, PlatformRepository platformRepository, UserRepository userRepository) {
        this.userRepository = userRepository;
        logger.debug("Creating repositories SeedData");
        this.developerRepository = developerRepository;
        this.gameRepository = gameRepository;
        this.platformRepository = platformRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        logger.debug("Running seed data");

        List<Platform> platforms = generatePlatforms();
        List<Developer> developers = generateDevelopers();
        List<Game> games = generateGames(platforms, developers);

        List<Developer> dbDevelopers = developerRepository.findAll();
        List<Game> dbGames = gameRepository.findAll();
        List<Platform> dbPlatforms = platformRepository.findAll();

        User user1 = new User("roby", "$2a$10$2.XK4u6vJkGaWXtK975rwO4iO./dXF7OcwGI6YRYBHzD729FkepqG", Role.USER); //password
        User user2 = new User("valve", "$2a$10$RRo6.B/3ykY872X87HvPLeZh3QrmvpQtO8OMqTsLwTxg5AHRKbLUG", Role.USER); //password123
        User user3 = new User("admin", "$2a$10$IxZHhDmkTjzZxQEcpWsBl.zVVJkOBKR26mfomYGu0reqoJlUXTJdq", Role.ADMIN); //admin

        userRepository.saveAll(List.of(user1, user2, user3));

        //testing logger output
//        logger.debug("Developer: " + dbDevelopers.get(0));
//        logger.debug("Developed games: " + dbDevelopers.get(0).getGames());
//        //logger.debug("Developed games: " + developerRepository.read().get(0).getGames());
//        logger.debug("Game: " + dbGames.get(0));
//        //logger.debug("Game: " + gameRepository.read().get(0));
//        logger.debug("Developed by: " + dbGames.get(0).getDevelopers());
//        //logger.debug("Developed by: " + gameRepository.read().get(0).getDevelopers());
//        logger.debug("Is on these platforms: " + dbGames.get(0).getPlatforms());
//        //logger.debug("Is on these platforms: " + gameRepository.read().get(0).getPlatforms());
//        logger.debug("Platform: " + dbPlatforms.get(0));
//        //logger.debug("Platform: " + platformRepository.read().get(0));
    }

     public List<Platform> generatePlatforms() {
        return platformRepository.saveAll(
           List.of(
                   new Platform("Windows","WIN","Microsoft",10),
                   new Platform ("Playstation","PS","Sony",5),
                   new Platform ("XBOX","XB","Microsoft",5),
                   new Platform ("Switch", "SW","Nintendo",1)
           )
        );
     }

     public List<Developer> generateDevelopers() {
          return developerRepository.saveAll(
               List.of(
                       new Developer("Blizzard", LocalDate.of(1962,10,3)),
                       new Developer("Activision", LocalDate.of(1969,4,21)),
                       new Developer("Valve", LocalDate.of(1972,11,10)),
                       new Developer("CD Projekt Red", LocalDate.of(1980,8,17)),
                       new Developer("Bungie", LocalDate.of(1963,10,12)),
                       new Developer("Riot Games", LocalDate.of(2016,8,8))
             )
          );
     }

     public List<Game> generateGames(List<Platform> platforms, List<Developer> developers) {
        Developer blizzard = developers.get(0);
        Developer activision = developers.get(1);
        Developer valve = developers.get(2);
        Developer cdProjektRed = developers.get(3);
        Developer bungie = developers.get(4);
        Developer riotGames = developers.get(5);

        Platform windows = platforms.get(0);
        Platform playstation = platforms.get(1);
        Platform xbox = platforms.get(2);
        Platform switchConsole = platforms.get(3);

        return gameRepository.saveAll(
                List.of(
                        new Game("Counter-Strike: Global Offensive", 9.5, LocalDate.of(2012,8,21), List.of(Genres.ACTION, Genres.FIRST_PERSON, Genres.SHOOTER), List.of(windows), valve),
                        new Game("The Witcher 3", 9, LocalDate.of(2015,5,18), List.of(Genres.ACTION, Genres.ADVENTURE, Genres.FIGHTING, Genres.RPG), List.of(windows), cdProjektRed),
                        new Game("League Of Legends", 10, LocalDate.of(2009,10,27), List.of(Genres.FIGHTING, Genres.RPG, Genres.ACTION), List.of(playstation, xbox), riotGames),
                        new Game("Halo 3", 9.5, LocalDate.of(2012,8,21), List.of(Genres.ACTION, Genres.FIRST_PERSON, Genres.SHOOTER), List.of(switchConsole, playstation), bungie),
                        new Game("Overwatch", 8, LocalDate.of(2016,5,24), List.of(Genres.ACTION, Genres.FIRST_PERSON, Genres.SHOOTER), List.of(windows), blizzard)
             )
        );
    }
}
