package be.kdg.java2.projectProgJava23.services;

import be.kdg.java2.projectProgJava23.domain.Platform;

import java.util.List;

public interface PlatformService {
    List<Platform> showAllPlatforms();
    Platform addPlatform(Platform platform);
    Platform findPlatformById(int id);
}
