package be.kdg.java2.projectProgJava23.services;

import be.kdg.java2.projectProgJava23.domain.Developer;

import java.util.List;

public interface DeveloperService {
    List<Developer> showAllDevelopers();
    Developer addDeveloper(Developer developer);
    Developer findDeveloperById(int id);
    List<Developer> showAllDeveloperGames();
    List<Developer> findByName(String firstName);
    List<Developer> searchDevelopers(String partOfDevName);
    void deleteDeveloper(int id);
    void updateDeveloper(Developer developer);
}
