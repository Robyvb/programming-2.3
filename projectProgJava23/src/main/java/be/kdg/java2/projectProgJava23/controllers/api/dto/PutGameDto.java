package be.kdg.java2.projectProgJava23.controllers.api.dto;

public class PutGameDto {
    private long id;
    private String name;
    private double rating;

    public PutGameDto() {
    }

    public PutGameDto(long id, String name, double rating) {
        this.id = id;
        this.name = name;
        this.rating = rating;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }
}
