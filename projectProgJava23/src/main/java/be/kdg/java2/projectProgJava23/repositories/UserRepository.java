package be.kdg.java2.projectProgJava23.repositories;

import be.kdg.java2.projectProgJava23.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
}
