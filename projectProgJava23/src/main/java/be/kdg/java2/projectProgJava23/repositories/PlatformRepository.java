package be.kdg.java2.projectProgJava23.repositories;

import be.kdg.java2.projectProgJava23.domain.Platform;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlatformRepository extends JpaRepository<Platform, Integer> {
}
