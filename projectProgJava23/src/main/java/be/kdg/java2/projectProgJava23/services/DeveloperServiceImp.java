package be.kdg.java2.projectProgJava23.services;

import be.kdg.java2.projectProgJava23.domain.Developer;
import be.kdg.java2.projectProgJava23.domain.Game;
import be.kdg.java2.projectProgJava23.repositories.DeveloperRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class DeveloperServiceImp implements DeveloperService{
    private final DeveloperRepository developerRepository;
    private static final Logger logger = LoggerFactory.getLogger(DeveloperServiceImp.class);
    @Autowired
    public DeveloperServiceImp(DeveloperRepository gameRepository){
        this.developerRepository = gameRepository;
    }

    @Override
    public List<Developer> showAllDevelopers() {
        return developerRepository.findAll();
    }

    @Override
    public List<Developer> showAllDeveloperGames(){
        return developerRepository.findAll();
    }

    @Override
    public List<Developer> findByName(String name) {
        return developerRepository.findByName(name);
    }

    @Override
    public Developer addDeveloper(Developer developer) {
        logger.debug("Adding developer: " + developer.getName());
        return developerRepository.save(developer);
    }

    @Override
    public Developer findDeveloperById(int id){
        logger.debug("Finding developer by id " + id);
        return developerRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("error"));
    }

    @Override
    public List<Developer> searchDevelopers(String partOfDevName){
        return this.developerRepository.findByNameContainsIgnoreCase(partOfDevName);
    }

    @Override
    public void deleteDeveloper(int id){
        Developer developerToBeDeleted = developerRepository.findById(id).orElseThrow();
        developerToBeDeleted.getGames().forEach(Game::removeDeveloper);
        developerRepository.delete(developerToBeDeleted);
    }

    @Override
    public void updateDeveloper(Developer developer) {
        developerRepository.save(developer);
    }
}
