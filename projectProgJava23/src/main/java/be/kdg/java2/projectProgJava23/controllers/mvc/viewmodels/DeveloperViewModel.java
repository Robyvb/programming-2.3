package be.kdg.java2.projectProgJava23.controllers.mvc.viewmodels;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.List;

public class DeveloperViewModel {

    @NotEmpty(message = "Please fill in the first name")
    @Size(max = 70, message = "Name has a maximum length of 70")
    private String name;

    @NotNull(message = "Please fill in the birth date")
    @Past(message = "The birthdate must be in the past")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthDate;

    //can be empty
    private List<Integer> games;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public List<Integer> getGames() {
        return games;
    }

    public void setGames(List<Integer> games) {
        this.games = games;
    }

    @Override
    public String toString() {
        return "DeveloperDTO{" +
                "name='" + name + '\'' +
                ", birthDate=" + birthDate +
                ", games=" + games +
                '}';
    }
}
