package be.kdg.java2.projectProgJava23.controllers.api;

import be.kdg.java2.projectProgJava23.controllers.api.dto.AddDeveloperDto;
import be.kdg.java2.projectProgJava23.controllers.api.dto.DeveloperDto;
import be.kdg.java2.projectProgJava23.domain.Developer;
import be.kdg.java2.projectProgJava23.security.AdminOnly;
import be.kdg.java2.projectProgJava23.security.CustomUserDetails;
import be.kdg.java2.projectProgJava23.services.DeveloperService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/developers")
public class DevelopersController {
    private final static Logger logger = LoggerFactory.getLogger(DevelopersController.class);
    private final DeveloperService developerService;

    @Autowired
    public DevelopersController(DeveloperService developerService) {
        this.developerService = developerService;
    }

    @GetMapping
    public ResponseEntity<List<DeveloperDto>> searchDevelopers(@RequestParam("partOfName") String partOfName){
        List<Developer> developers = developerService.searchDevelopers(partOfName);
        if (developers.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }else{
            var developerDtos = developers
                    .stream()
                    .map(developer ->{
                        var dto = new DeveloperDto();
                        dto.setId(developer.getId());
                        dto.setName(developer.getName());
                        return dto;
                    }).collect(Collectors.toList());
            return new ResponseEntity<>(developerDtos, HttpStatus.OK);
        }
    }

    @GetMapping("{id}")
    public ResponseEntity<DeveloperDto> retrieveDeveloper(@PathVariable(value = "id") int id){
        Developer developer = developerService.findDeveloperById(id);
        if(developer == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new DeveloperDto(developer.getId(), developer.getName()), HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    @AdminOnly
    public ResponseEntity<Void> deleteDeveloper(@PathVariable(value = "id") int id){
        var developerToDelete = developerService.findDeveloperById(id);
        if (developerToDelete == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        developerService.deleteDeveloper(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PutMapping("{id}")
    @AdminOnly
    public ResponseEntity<Void> updateDeveloper (
            @PathVariable(value = "id") int id,
            @RequestBody DeveloperDto developerDto){
        if (id != developerDto.getId()){
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        Developer developer = developerService.findDeveloperById(id);
        if(developer == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        developer.setName(developerDto.getName());

        developerService.updateDeveloper(developer);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PostMapping
    @AdminOnly
    public ResponseEntity<Void> addDeveloper(
            @RequestBody AddDeveloperDto addDeveloperDto,
            @AuthenticationPrincipal CustomUserDetails customUserDetails){
        Developer developer = new Developer();

        //TODO: checkbox with lists of GameDtos

        if(addDeveloperDto.getName() == null || addDeveloperDto.getFoundedDate() == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        developer.setName(addDeveloperDto.getName());
        developer.setFoundedDate(addDeveloperDto.getFoundedDate());

        developerService.addDeveloper(developer);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}