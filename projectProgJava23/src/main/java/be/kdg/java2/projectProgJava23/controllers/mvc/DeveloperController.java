package be.kdg.java2.projectProgJava23.controllers.mvc;

import be.kdg.java2.projectProgJava23.domain.Developer;
import be.kdg.java2.projectProgJava23.services.DeveloperService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/developers")
public class DeveloperController{
    private final Logger logger = LoggerFactory.getLogger(DeveloperController.class);
    private final DeveloperService developerService;

    public DeveloperController(DeveloperService developerService){
        logger.debug("Creating developerController");
        this.developerService = developerService;
    }

    @GetMapping
    public String showAllDevelopers(Model model){
        logger.debug("Running showAllDevelopers");
        model.addAttribute("developers", developerService.showAllDevelopers());
        return "developers";
    }

    @GetMapping("/detailsDeveloper")
    public String detailsDeveloper(@RequestParam("developerID") Integer developerID, Model model){
        //find the selected developer and show it
        Developer requestedDeveloper = developerService.findDeveloperById(developerID);
        model.addAttribute("developer", requestedDeveloper);
        return "detailsDeveloper";
    }

    @GetMapping("/searchDeveloper")
    public String searchForDeveloper(){
        return "searchDeveloper";
    }
}
