package be.kdg.java2.projectProgJava23.domain;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "games")
public class Game extends EntityDomain{
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @Column(name = "game_name")
    private String name;
    @Column(name = "game_rating")
    private double rating;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "game_release_date")
    private LocalDate releaseDate;

    @ElementCollection(fetch = FetchType.EAGER)
    @Enumerated(EnumType.STRING)
    private List<Genres> genres;

    @ManyToMany
    @JoinTable(name="platform_game",
            joinColumns = @JoinColumn(name= "game_id"),
            inverseJoinColumns = @JoinColumn(name="platform_id"))
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Platform> platforms;

    @ManyToOne
    @JoinColumn
    private Developer developer;

    public Game(String name, double rating, LocalDate releaseDate, List<Genres> genres) {
        this.name = name;
        this.rating = rating;
        this.releaseDate = releaseDate;
        this.genres = genres;
        this.platforms = new ArrayList<>();
        this.developer = new Developer();
    }

    public Game(String name, double rating, LocalDate releaseDate, List<Genres> genres, List<Platform> platforms) {
        this.name = name;
        this.rating = rating;
        this.releaseDate = releaseDate;
        this.genres = genres;
        this.platforms = platforms;
        this.developer = new Developer();
    }

    public Game(String name, double rating, LocalDate releaseDate, List<Genres> genres, List<Platform> platforms, Developer developer) {
        this.name = name;
        this.rating = rating;
        this.releaseDate = releaseDate;
        this.genres = genres;
        this.platforms = platforms;
        this.developer = developer;
    }

    public Game(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Developer getDeveloper() {
        return developer;
    }

    public void setDeveloper(Developer developer) {
        this.developer = developer;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    public List<Genres> getGenres() {
        return genres;
    }

    public void setGenres(List<Genres> genres) {
        this.genres = genres;
    }

    public List<Platform> getPlatforms() {
        return platforms;
    }

    public void setPlatforms(List<Platform> platforms) {
        this.platforms = platforms;
    }

    public String returnDeveloperName(){
        if(developer == null) return "";
        return developer.getName();
    }

    public void removeDeveloper(){
        //set developer to null because it can only have one
        this.developer = null;
    }

    @Override
    public String toString() {
        return name;
    }
}
