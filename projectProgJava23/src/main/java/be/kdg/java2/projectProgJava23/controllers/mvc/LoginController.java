package be.kdg.java2.projectProgJava23.controllers.mvc;

import be.kdg.java2.projectProgJava23.controllers.mvc.viewmodels.LoginViewModel;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginController {
    @GetMapping("/login")
    public ModelAndView login(ModelAndView mav) {
        mav.setViewName("login");
        mav.addObject("loginInfo", new LoginViewModel());
        return mav;
    }
}