package be.kdg.java2.projectProgJava23.controllers.api;

import be.kdg.java2.projectProgJava23.controllers.api.dto.AddGameDto;
import be.kdg.java2.projectProgJava23.controllers.api.dto.DeveloperDto;
import be.kdg.java2.projectProgJava23.controllers.api.dto.GameDto;
import be.kdg.java2.projectProgJava23.controllers.api.dto.PutGameDto;
import be.kdg.java2.projectProgJava23.domain.Developer;
import be.kdg.java2.projectProgJava23.domain.Game;
import be.kdg.java2.projectProgJava23.security.AdminOnly;
import be.kdg.java2.projectProgJava23.services.GameService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/games")
public class GamesController {
    private final Logger logger = LoggerFactory.getLogger(GamesController.class);
    private final GameService gameService;

    @Autowired
    public GamesController(GameService gameService) {
        this.gameService = gameService;
    }

    @GetMapping
    public ResponseEntity<List<GameDto>> searchGames(@RequestParam("name") String partOfName){
        List<Game> games = gameService.searchGames(partOfName);
        if (games.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }else{
            var gameDtos = games
                    .stream()
                    .map(game ->{
                        var dto = new GameDto();
                        dto.setId(game.getId());
                        dto.setName(game.getName());
                        dto.setRating(game.getRating());
                        dto.setReleaseDate(game.getReleaseDate());
                        return dto;
                    }).collect(Collectors.toList());
            return new ResponseEntity<>(gameDtos, HttpStatus.OK);
        }
    }

    @GetMapping("{id}")
    public ResponseEntity<PutGameDto> retrieveGame(@PathVariable(value = "id") int id){
        Game game = gameService.findGameById(id);
        if(game == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new PutGameDto(game.getId(), game.getName(),game.getRating()), HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    @AdminOnly
    public ResponseEntity<Void> deleteGame(@PathVariable(value = "id", required = false) int id){
        Game gameToDelete = gameService.findGameById(id);
        if (gameToDelete != null){
            gameService.deleteGame(id);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PutMapping("{id}")
    @AdminOnly
    public ResponseEntity<Void> updateDeveloper (
            @PathVariable(value = "id") int id,
            @RequestBody PutGameDto putGameDto){
        if (id != putGameDto.getId()){
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        Game game = gameService.findGameById(id);
        if(game == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        game.setName(putGameDto.getName());
        game.setRating(putGameDto.getRating());

        gameService.updateGame(game);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PostMapping
    @AdminOnly
    public ResponseEntity<Void> addGame(
            @RequestBody GameDto addGameDto){
        Game game = new Game();

        //TODO: developer

        if(addGameDto.getName() == null || addGameDto.getRating() == 0 || addGameDto.getReleaseDate() == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        game.setName(addGameDto.getName());
        game.setRating(addGameDto.getRating());
        game.setReleaseDate(addGameDto.getReleaseDate());

        logger.debug("New game: " + game);
        gameService.addGame(game);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
