package be.kdg.java2.projectProgJava23.controllers.mvc;

import be.kdg.java2.projectProgJava23.controllers.mvc.viewmodels.GameViewModel;
import be.kdg.java2.projectProgJava23.domain.*;
import be.kdg.java2.projectProgJava23.services.DeveloperService;
import be.kdg.java2.projectProgJava23.services.GameService;
import be.kdg.java2.projectProgJava23.services.PlatformService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/games")
public class GameController {
    private final Logger logger = LoggerFactory.getLogger(GameController.class);
    private final GameService gameService;

    public GameController(GameService gameService) {
        logger.debug("Creating gameController");
        this.gameService = gameService;
    }

    @GetMapping
    public String showAllGames(Model model){
        logger.debug("Running showAllGames");
        model.addAttribute("games", gameService.showAllGames());
        return "games";
    }

    @GetMapping("/detailsGame")
    public String detailsGame(@RequestParam("gameID") Integer gameID, Model model){
        Game requestedGame = gameService.findGameById(gameID);
        model.addAttribute("game", requestedGame);
        return "detailsGame";
    }

    @GetMapping("/searchGame")
    public String searchGame(){
        return "searchGame";
    }
}
