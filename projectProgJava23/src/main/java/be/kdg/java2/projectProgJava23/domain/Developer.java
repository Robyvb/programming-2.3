package be.kdg.java2.projectProgJava23.domain;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "developers")
public class Developer extends EntityDomain{
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @Column(length=100)
    private String name;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate foundedDate;

    @OneToMany(mappedBy = "developer")
    private List<Game> games;

    public Developer(String name, LocalDate foundedDate) {
        this.name = name;
        this.foundedDate = foundedDate;
    }

    public Developer(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getFoundedDate() {
        return foundedDate;
    }

    public void setFoundedDate(LocalDate foundedDate) {
        this.foundedDate = foundedDate;
    }

    public List<Game> getGames() {
        return games;
    }

    public void setGames(List<Game> games) {
        this.games = games;
    }

    public void removeAllGames(){
        this.games.clear();
    }

    public void removeGame(Game game){
        this.games.remove(game);
    }

    @Override
    public String toString() {
        return name;
    }
}
