package be.kdg.java2.projectProgJava23.configuration;

import be.kdg.java2.projectProgJava23.controllers.mvc.viewmodels.DeveloperViewModel;
import be.kdg.java2.projectProgJava23.controllers.mvc.viewmodels.GameViewModel;
import be.kdg.java2.projectProgJava23.domain.Developer;
import be.kdg.java2.projectProgJava23.domain.Game;
import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.modelmapper.ModelMapper;

@Configuration
public class ProjectProgJava23Configuration {
    @Bean
    public ModelMapper modelMapper(){
        var modelMapper = new ModelMapper();

        Converter<Developer, DeveloperViewModel> developerConverter = new AbstractConverter<>() {
            @Override
            protected DeveloperViewModel convert(Developer source) {
                if (source == null) return null;

                DeveloperViewModel destination = new DeveloperViewModel();

                destination.setName(source.getName());
                destination.setBirthDate(source.getFoundedDate());

                return destination;
            }
        };

        Converter<Game, GameViewModel> gameConverter = new AbstractConverter<>() {
            @Override
            protected GameViewModel convert(Game source) {
                if (source == null) return null;

                GameViewModel destination = new GameViewModel();

                destination.setGenres(source.getGenres());
                destination.setName(source.getName());
                destination.setRating(source.getRating());
                destination.setReleaseDate(source.getReleaseDate());

                return destination;
            }
        };

        modelMapper.addConverter(developerConverter);
        modelMapper.addConverter(gameConverter);

        return modelMapper;
    }
}
