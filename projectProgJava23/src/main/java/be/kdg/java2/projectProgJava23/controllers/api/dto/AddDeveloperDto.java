package be.kdg.java2.projectProgJava23.controllers.api.dto;

import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

public class AddDeveloperDto {
    private String name;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate foundedDate;

    public AddDeveloperDto() {
    }

    public AddDeveloperDto(String name, LocalDate foundedDate) {
        this.name = name;
        this.foundedDate = foundedDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getFoundedDate() {
        return foundedDate;
    }

    public void setFoundedDate(LocalDate foundedDate) {
        this.foundedDate = foundedDate;
    }
}
