package be.kdg.java2.projectProgJava23.repositories;

import be.kdg.java2.projectProgJava23.domain.Developer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DeveloperRepository extends JpaRepository<Developer, Integer> {
    List<Developer> findByName(String name);
    List<Developer> findByNameContainsIgnoreCase(String name);
}
