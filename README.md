# Programming 2.3 - Project

Roby Van Boxelaere  
roby.vanboxelaere@student.kdg.be  
0149280-94  

This is an ongoing project for Programming 2.3  
This project is continued from my Programming 2.1 project

## Domain classes

A developer can have multiple games.  
A game can have one developer and contains multiple platforms.

## Project details

- Java version: JDK 11
- Database: H2, no need to do anything here
- Url: Localhost:8080

## Week 1

The two implemented REST endpoints: GET and DELETE
There are two ways of using GET:

- partOfName: this is used in the project to search developers based on their name
- id: this is not used in the project, although it's used to test other endpoints such as delete
The requests in this file are only for developers. In the file "httpTest.http" you can find all requests, including the ones for side of game.

### GET

#### Using id

GET http://localhost:8080/api/games/11 HTTP/1.1  

GET http://localhost:8080/api/developers/5 HTTP/1.1  

#### 200 - OK

GET http://localhost:8080/api/developers?partOfName=Valve HTTP/1.1  
Accept: application/json  

```
[
  {
    "id": 7,
    "name": "Valve"
  }
]
```

#### 204 - Empty response body

GET http://localhost:8080/api/developers?partOfName=fudi HTTP/1.1  
Accept: application/json  

```
<Response body is empty>
```

#### 400 - Bad Request

GET http://localhost:8080/api/developers?nam=Valve HTTP/1.1  
Accept: application/json  

```
{
  "timestamp": "2022-02-26T12:37:40.672+00:00",
  "status": 400,
  "error": "Bad Request",
  "path": "/api/developers"
}
```

#### 404 - Page not found

GET http://localhost:8080/api/developer?name=Valve HTTP/1.1  
Accept: application/json  

```
{
  "timestamp": "2022-02-26T12:34:31.071+00:00",
  "status": 404,
  "error": "Not Found",
  "path": "/api/developer"
}
```

### DELETE

#### 204 - No content

DELETE http://localhost:8080/api/developers/5 HTTP/1.1  

```
<Response body is empty>
```

#### 500 - Internal server error

DELETE http://localhost:8080/api/developers/74 HTTP/1.1  

```
{
  "timestamp": "2022-02-26T12:39:37.853+00:00",
  "status": 500,
  "error": "Internal Server Error",
  "path": "/api/developers/74"
}
```

## Week 2

The two implemented REST endpoints: PUT and POST  
I also implemented content negotiation. You can retrieve games in XML format  

### PUT

#### 204 - No content

PUT http://localhost:8080/api/developers/5 HTTP/1.1  
Content-Type: application/json

```
<Response body is empty>
```

#### 400 - Bad Request

PUT http://localhost:8080/api/developers/5 HTTP/1.1  
Content-Type: application/json  
```
{
  "id": 5,
}
```

```
{
  "timestamp": "2022-02-26T13:02:17.186+00:00",
  "status": 400,
  "error": "Bad Request",
  "path": "/api/developers/5"
}
```

#### 409 - Conflict

PUT http://localhost:8080/api/developers/5 HTTP/1.1  
Content-Type: application/json  

```
{
  "id": 89,
  "name": "New name"
}
```

```
<Response body is empty>
```

### POST

#### 201 - Created

POST http://localhost:8080/api/developers HTTP/1.1  
Accept: application/json  
Content-Type: application/json  

```
{
  "name":"New developer",
  "foundedDate":"2022-02-01"
}
```

```
<Response body is empty>
```

#### 400 - Bad Request

POST http://localhost:8080/api/developers HTTP/1.1  
Accept: application/json  
Content-Type: application/json  

{
"name": "New developer"
}

```
<Response body is empty>
```

#### 404 - Page not found

POST http://localhost:8080/api/developer HTTP/1.1  
Accept: application/json  
Content-Type: application/json  

```
{
  "name":"New developer",
  "foundedDate":"2022-02-01"
}
```

```
{
  "timestamp": "2022-02-26T13:03:51.090+00:00",
  "status": 404,
  "error": "Not Found",
  "path": "/api/developer"
}
```

## Week 3
For week 3, I made use of npm and webpack.  
I also switched over from CSS to SCSS (although there was little self-written CSS).  
I made use of Bootstrap Icons. These are located in thsearchX pages.
- http://localhost:8080/developers/searchDevelope(searchDeveloper.html)
- http://localhost:8080/developers/searchGame (searchGamhtml)  

Two JavaScript dependencies were used.
- **Validation**
    - Validation was used in the pages where you adentities (add form at the bottom of the page):
        - http://localhost:8080/games (games.html - addGamjs)
        - http://localhost:8080/developers (developers.htm- addDeveloper.js)
- **Flatpickr**
    - Flatpickr was also used in the pages where you adentities (date picker in the add form at the bottom othe page):
        - http://localhost:8080/games (games.html flatpickrSetup.js)
        - http://localhost:8080/developers (developers.htm- flatpickrSetup.js)
